# from django.shortcuts import render
from django.http import HttpResponse # JsonResponse
from .models import Project, Task
from django.shortcuts import render, get_object_or_404
from .forms import CreateNewTask

# Create your views here.
def index(request):
    title = "Welcome to Django Course"
    return render(request, "index.html", {
        "title": title
    })
    
def hello(request, username):
    return HttpResponse("<h1>Hello %s</h1>" % username)

def about(request):
    # return HttpResponse("About")
    username = "Arturo"
    return render(request, "about.html", {
        "username": username
    })

def projects(request):
    # projects = list(Project.objects.values())
    # return JsonResponse(projects, safe=False)
    projects = Project.objects.all()
    return render(request, 'projects.html', {
        "projects": projects
    })

def tasks(request):
    # task = Task.objects.get(id=id)
    # task = get_object_or_404(Task, id=id)
    # task = Task.objects.get(title__startswith=title)
    # return HttpResponse('task: %s' % task.title)
    tasks = Task.objects.all()
    return render(request, 'tasks.html', {
        "tasks": tasks
    })

def create_task(request):
    return render(request, 'create_task.html', {
        'form': CreateNewTask()
    })